# Copyright (C) 2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

# This is a Docker image for *experimental purposes* with a Solid
# server running on localhost: https://solid.localhost:8443
#
# See there for production settings:
# https://github.com/angelo-v/docker-solid-server/blob/master/src/Dockerfile

FROM node:10-alpine

# openssl is useful to create certificates; for testing purposes, I like
# telnet (in busybox-extras) and gnutls-cli (in gnutls-utils).
# I use dnsmasq for wildcard DNS names.  How to do without?
RUN apk add --no-cache openssl busybox-extras gnutls-utils dnsmasq

# Install Solid.
RUN npm install -g solid-server@5.0.1

# Copy configuration to /tmp.  Run containers with option to mount host
# directory: -v some-host-directory:/opt/solid
COPY ./entrypoint.sh /tmp/
COPY ./config.json /tmp/
COPY ./certs/*crt /tmp/certs/

# Start shells in Solid directory.
WORKDIR /opt/solid

# Activate all debug flags.
# Set up DNS and certificates inside the container in entrypoint.sh.
ENV DEBUG=solid:*
ENTRYPOINT ["/tmp/entrypoint.sh"]

# Use config file for start of Solid.
CMD ["start", "--config-file", "/tmp/config.json"]
